const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

let renderColorButtons = () => {
    let contentHTML = ``;
    for (index = 0; index < colorList.length; index++){
        let colorButton = `
            <button onclick="changeColor('${colorList[index]}')" class="color-button ${colorList[index]}"></button>
        `;
        contentHTML += colorButton;
    }
    document.getElementById("colorContainer").innerHTML = contentHTML;
    document.getElementsByClassName("color-button")[0].classList.add("active");
}
renderColorButtons();


let changeColor = (color) => {
    document.getElementById("house").className = `house ${color}`;
    var btns = document.getElementsByClassName("color-button");
    for (var i = 0; i < btns.length; i++) {
        btns[i].classList.remove("active");
        btns[i].addEventListener("click", function() {
            this.classList.add("active");
        });
    }
}
