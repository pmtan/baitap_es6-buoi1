let tinhDTB = (...input) =>{
    let sum = 0;
    for(index = 0; index < input.length; index++){
        sum += input[index];
    }
    return (sum/input.length).toFixed(2);
}
document.getElementById("btnKhoi1").addEventListener("click", () =>{
    let toan = document.getElementById("inpToan").value * 1;
    let ly = document.getElementById("inpLy").value * 1;
    let hoa = document.getElementById("inpHoa").value * 1;
    let dtbKhoi1 = tinhDTB(toan,ly,hoa);
    document.getElementById("tbKhoi1").innerHTML = dtbKhoi1;
})
document.getElementById("btnKhoi2").addEventListener("click", () => {
    let van = document.getElementById("inpVan").value * 1;
    let su = document.getElementById("inpSu").value * 1;
    let dia = document.getElementById("inpDia").value * 1;
    let english = document.getElementById("inpEnglish").value * 1;
    let dtbKhoi2 = tinhDTB(van, su, dia,english);
    document.getElementById("tbKhoi2").innerHTML = dtbKhoi2;
});
