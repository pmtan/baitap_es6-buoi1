let hoverEffect = () => {
    let text = document.querySelector(".heading").innerHTML;
    console.log(text);
    let chars = [...text];
    let contentHTML = "";
    chars.forEach(char => {
        let charHTML = `
            <span>${char}</span>
        `
        contentHTML+= charHTML;
    })
    document.querySelector(".heading").innerHTML = contentHTML;
}
hoverEffect();